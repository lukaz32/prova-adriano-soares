//
//  CharacterListViewModel.swift
//  MarvelApp
//
//  Created by Adriano Soares on 31/01/18.
//  Copyright © 2018 Adriano Soares. All rights reserved.
//

import Foundation

class CharacterListViewModel {
    var api = MarvelAPI() // FIXME: ⚠️ Poderia ser let ao invés de var
    // FIXME: ✅ weak delegate, nice
    weak var delegate: CharacterListViewModelDelegate?
    var offset = 0 // FIXME: ⚠️(2x) Poderia ser let ao invés de var
    var characters = [CharacterModel]()
    
    fileprivate var isLoading = false
    
    func fetchCharacterList () {
        if isLoading {
            return
        }
        isLoading = true
        api.fetchCharacters(offset: offset) { characterList in // FIXME: ❌ captura strong do self dentro da Closure
            self.offset = characterList.offset + characterList.limit
            self.characters.append(contentsOf: characterList.characters)
            self.delegate?.didUpdateCharacterList(list: characterList.characters)
            self.isLoading = false
        }
    }
    
}

protocol CharacterListViewModelDelegate: class {
    func didUpdateCharacterList (list: [CharacterModel])
}

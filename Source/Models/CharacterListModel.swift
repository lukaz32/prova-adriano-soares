//
//  CharacterListModel.swift
//  MarvelApp
//
//  Created by Adriano Soares on 31/01/18.
//  Copyright © 2018 Adriano Soares. All rights reserved.
//

import Foundation
import ObjectMapper

// FIXME: ⚠️ Poderia ser uma struct
class CharacterListModel: Mappable {
    var count: Int! // FIXME: ⚠️ Poderia ser um let e sem force-unwrapping
    var limit: Int!
    var offset: Int!
    var characters = [CharacterModel]()
    
    required init?(map: Map) {
        // FIXME: ⚠️ Não deveria haver uma imprementecao nesse init?
    }
    
    func mapping(map: Map) {
        count      <- map["data.total"]
        limit      <- map["data.limit"]
        offset     <- map["data.offset"]
        characters <- map["data.results"]
    }
    
}
